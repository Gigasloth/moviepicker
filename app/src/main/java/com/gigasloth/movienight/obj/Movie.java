package com.gigasloth.movienight.obj;

public class Movie {
    private int movieId;
    private String imdbId;
    private String submitter;
    private String title;
    private String releaseDate;
    private String posterPath;
    private boolean watched;

    // For SugarORM, do not use
    public Movie() {
    }

    public Movie(int movieId, String imdbId, String submitter, String title, String releaseDate, String posterPath) {
        this.movieId = movieId;
        this.imdbId = imdbId;
        this.submitter = submitter;
        this.title = title;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
    }

    public int getMovieId() {
        return movieId;
    }

    public String getImdbId() {
        return imdbId;
    }

    public String getSubmitter() {
        return submitter;
    }

    public void setSubmitter(String submitter) {
        this.submitter = submitter;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public boolean isWatched() {
        return watched;
    }

    public void setWatched(boolean watched) {
        this.watched = watched;
    }
}
