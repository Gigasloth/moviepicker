package com.gigasloth.movienight;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.gigasloth.movienight.util.LruBitmapCache;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.config.TmdbConfiguration;

public class MovieNight extends Application {
    public static final String TAG = MovieNight.class.getSimpleName();
    public static final String API_KEY = "";
    public static final String PREF_KEY_USERNAME = "user";
    public static TmdbApi tmdbApi;
    public static TmdbConfiguration config;
    public static Context context;

    private static MovieNight instance;
    private static SharedPreferences prefs;
    private RequestQueue queue;
    private ImageLoader imageLoader;
    private static Firebase database;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Firebase.setAndroidContext(this);
        database = new Firebase("https://movienightpicker.firebaseio.com/");

        // Authenticate The Movie Database API Key
        new Thread(new Runnable() {
            @Override
            public void run() {
                tmdbApi = new TmdbApi(API_KEY);
                config = tmdbApi.getConfiguration();
            }
        }).start();
        context = this;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPreferences getPrefs() {
        return prefs;
    }

    public static Firebase getMoviesRef() {
        return database.child(DbRef.MOVIES);
    }

    public static Firebase getCurrentMovieRef() {
        return database.child(DbRef.CURRENT_MOVIE);
    }

    public static MovieNight getInstance() {
        return instance;
    }

    /**
     * Returns the <code>RequestQueue</code> singleton
     *
     * @return
     *      The existing request queue, or a new one if it didn't exist
     */
    public RequestQueue getRequestQueue() {
        if (queue == null) {
            queue = Volley.newRequestQueue(getApplicationContext());
        }

        return queue;
    }

    /**
     * Returns the <code>ImageLoader</code> singleton
     *
     * @return
     *      The existing image loader, or a new one if it didn't exist
     */
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (imageLoader == null) {
            imageLoader = new ImageLoader(this.queue, new LruBitmapCache());
        }

        return this.imageLoader;
    }

    /**
     * Adds a new <code>Request</code> to the queue
     *
     * @param req
     *          The request object to add
     * @param tag
     *          A tag for the request to be referenced in the queue
     * @param <T>
     *          Request return type
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    /**
     * Adds a new <code>Request</code> to the queue
     *
     * @param req
     *          The request object to add
     * @param <T>
     *          Request return type
     */
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    /**
     * Cancels pending requests with the given tag
     *
     * @param tag
     *          Cancels the requests marked with this tag
     */
    public void cancelPendingRequests(Object tag) {
        if (queue != null) {
            queue.cancelAll(tag);
        }
    }
}
