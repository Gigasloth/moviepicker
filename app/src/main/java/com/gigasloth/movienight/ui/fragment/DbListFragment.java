package com.gigasloth.movienight.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.gigasloth.movienight.MovieNight;
import com.gigasloth.movienight.R;
import com.gigasloth.movienight.obj.Movie;
import com.gigasloth.movienight.ui.component.DbListAdapter;

import java.util.ArrayList;

public class DbListFragment extends Fragment {
    private RecyclerView rvDbList;
    private ArrayList<Movie> movies;
    private DbListAdapter dbListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_db_list, container, false);

        rvDbList = (RecyclerView) rootView.findViewById(R.id.rv_db_list);

        movies = new ArrayList<>();
        rvDbList.setLayoutManager(new LinearLayoutManager(MovieNight.context));
        dbListAdapter = new DbListAdapter(movies);
        rvDbList.setAdapter(dbListAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        MovieNight.getMoviesRef().orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!movies.isEmpty()) {
                    movies.clear();
                }

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    movies.add(snapshot.getValue(Movie.class));
                }

                if (movies.isEmpty()) {
                    Toast.makeText(MovieNight.context, R.string.toast_empty_db, Toast.LENGTH_SHORT).show();
                    return;
                }

                dbListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    private void deleteMovie(Movie movie) {
        MovieNight.getMoviesRef().child(movie.getTitle() + " - " + movie.getMovieId()).removeValue(new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                String text;
                if (firebaseError != null) {
                    text = firebaseError.getMessage();
                } else {
                    text = "Movie deleted from db";
                }

                Toast.makeText(MovieNight.context, text, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
