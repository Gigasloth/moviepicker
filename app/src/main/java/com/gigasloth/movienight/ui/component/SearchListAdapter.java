package com.gigasloth.movienight.ui.component;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.gigasloth.movienight.MovieNight;
import com.gigasloth.movienight.R;
import com.gigasloth.movienight.obj.Movie;

import java.util.ArrayList;

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.MovieViewHolder> {
    private static ArrayList<Movie> movies = new ArrayList<>();
    private Movie movie;

    public SearchListAdapter(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    /**
     * This is where we bind the data to the views. This is called when the SO binds the view with
     * the data (when it's shown in the UI)
     *
     * @param movieViewHolder
     *      ViewHolder to be bound
     * @param pos
     *      Position in the data list
     */
    @Override
    public void onBindViewHolder(MovieViewHolder movieViewHolder, int pos) {
        movie = movies.get(pos);
        movieViewHolder.holderObj = movie;
        movieViewHolder.tvTitle.setText(movie.getTitle());
        movieViewHolder.tvReleaseDate.setText(movie.getReleaseDate());
        movieViewHolder.imgPosterImage.setImageBitmap(null);
        if (movie.getPosterPath() != null) {
            getPosterImage(movie, movieViewHolder.imgPosterImage);
        } else {
            movieViewHolder.imgPosterImage.setImageBitmap(null);
        }
    }

    /**
     * <code>ImageLoader</code> request for poster images. Loads the image from url or from cache if available.
     *
     * @param movie
     *          The movie object
     *
     * @param imageView
     *          The image view to set the poster image
     */
    private void getPosterImage(final Movie movie, final ImageView imageView) {
        MovieNight app = MovieNight.getInstance();
        String requestUrl = MovieNight.config.getSecureBaseUrl() + "/original/" + movie.getPosterPath();

        app.getImageLoader().get(requestUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer imageContainer, boolean isImmediate) {
                Bitmap bitmap;

                // ImageLoader's first pass checks the cache which will return null first time around
                if ((bitmap = imageContainer.getBitmap()) == null) {
                    return;
                }

                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VOLLEY", volleyError.toString());
            }
        });
    }

    /**
     * This method inflates a new card layout and builds a new ViewHolder object. This is called
     * whenever a new instance of our ViewHolder class is created.
     *
     * @param viewGroup
     *      ViewGroup to add the new view to
     * @param pos
     *      Position in the data list
     * @return
     *      A new ViewHolder
     */
    @Override
    public MovieViewHolder onCreateViewHolder(final ViewGroup viewGroup, int pos) {
        View itemView = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.list_item_search, viewGroup, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                View dialogView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.dialog_add_movie, null);

                final MovieViewHolder movieViewHolder = ((MovieViewHolder) v.getTag());

                final AlertDialog addDialog = new AlertDialog.Builder(viewGroup.getContext())
                        .setView(dialogView)
                        .setTitle(viewGroup.getContext().getString(R.string.alert_movie_title))
                        .setMessage(viewGroup.getContext().getString(R.string.alert_movie_description))
                        .setCancelable(true)
                        .setPositiveButton(viewGroup.getContext().getString(R.string.alert_btn_movie_add),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        MovieNight.getMoviesRef()
                                                .child(movieViewHolder.holderObj.getTitle() + " - " + movieViewHolder.holderObj.getMovieId())
                                                .setValue(movieViewHolder.holderObj, new Firebase.CompletionListener() {
                                                    @Override
                                                    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                                        if (firebaseError != null) {
                                                            Toast.makeText(MovieNight.context,
                                                                    "Data could not be saved. " + firebaseError.getMessage(),
                                                                    Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Toast.makeText(MovieNight.context,
                                                                    "Data saved successfully.",
                                                                    Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }
                                })
                        .setNegativeButton(viewGroup.getContext().getString(R.string.alert_btn_movie_cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                        .create();
                addDialog.setCanceledOnTouchOutside(true);

                TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_title_dialog);
                TextView tvReleaseDate = (TextView) dialogView.findViewById(R.id.tv_release_date_dialog);
                ImageView imgPosterImage = (ImageView) dialogView.findViewById(R.id.img_poster_dialog);

                tvTitle.setText(movieViewHolder.holderObj.getTitle());
                tvReleaseDate.setText(movieViewHolder.holderObj.getReleaseDate());
                getPosterImage(movieViewHolder.holderObj, imgPosterImage);
                //imgPosterImage.setImageBitmap(movieViewHolder.holderObj.getPosterImage()); //scaleBitmapToMedium(movieViewHolder.holderObj.getPosterImage()));

                addDialog.show();
            }
        });

        return new MovieViewHolder(itemView);
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder {
        protected Movie holderObj;
        protected TextView tvTitle;
        protected TextView tvReleaseDate;
        protected ImageView imgPosterImage;

        public MovieViewHolder(View view) {
            super(view);
            view.setTag(this);
            tvTitle = (TextView) view.findViewById(R.id.tv_title_search);
            tvReleaseDate = (TextView) view.findViewById(R.id.tv_release_date_search);
            imgPosterImage = (ImageView) view.findViewById(R.id.img_poster_thumbnail_search);
        }
    }

}
