package com.gigasloth.movienight.ui.component;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.gigasloth.movienight.MovieNight;
import com.gigasloth.movienight.R;
import com.gigasloth.movienight.obj.Movie;

import java.util.ArrayList;

public class DbListAdapter extends RecyclerView.Adapter<DbListAdapter.MovieViewHolder> {
    private static ArrayList<Movie> movies = new ArrayList<>();
    private Movie movie;

    public DbListAdapter(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    /**
     * This is where we bind the data to the views. This is called when the SO binds the view with
     * the data (when it's shown in the UI)
     *
     * @param movieViewHolder
     *      ViewHolder to be bound
     * @param pos
     *      Position in the data list
     */
    @Override
    public void onBindViewHolder(MovieViewHolder movieViewHolder, int pos) {
        movie = movies.get(pos);
        movieViewHolder.holderObj = movie;
        movieViewHolder.tvTitle.setText(movie.getTitle());
        movieViewHolder.tvReleaseDate.setText(movie.getReleaseDate());
        movieViewHolder.tvSubmitter.setText(movie.getSubmitter());
        movieViewHolder.imgPosterImage.setImageBitmap(null);
        if (movie.getPosterPath() != null) {
            getPosterImage(movie, movieViewHolder.imgPosterImage);
        } else {
            movieViewHolder.imgPosterImage.setImageBitmap(null);
        }

        if (movie.isWatched()) {
            movieViewHolder.imgWatched.setVisibility(View.VISIBLE);
        } else {
            movieViewHolder.imgWatched.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * <code>ImageLoader</code> request for poster images. Loads the image from url or from cache if available.
     *
     * @param movie
     *          The movie object
     *
     * @param imageView
     *          The image view to set the poster image
     */
    private void getPosterImage(final Movie movie, final ImageView imageView) {
        MovieNight app = MovieNight.getInstance();
        String requestUrl = MovieNight.config.getSecureBaseUrl() + "/original/" + movie.getPosterPath();

        app.getImageLoader().get(requestUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer imageContainer, boolean isImmediate) {
                Bitmap bitmap;

                // ImageLoader's first pass checks the cache which will return null first time around
                if ((bitmap = imageContainer.getBitmap()) == null) {
                    return;
                }

                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VOLLEY", volleyError.toString());
            }
        });
    }

    /**
     * This method inflates a new card layout and builds a new ViewHolder object. This is called
     * whenever a new instance of our ViewHolder class is created.
     *
     * @param viewGroup
     *      ViewGroup to add the new view to
     * @param pos
     *      Position in the data list
     * @return
     *      A new ViewHolder
     */
    @Override
    public MovieViewHolder onCreateViewHolder(final ViewGroup viewGroup, int pos) {
        View itemView = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.list_item_db_list, viewGroup, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

            }
        });

        return new MovieViewHolder(itemView);
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder {
        protected Movie holderObj;
        protected TextView tvTitle, tvReleaseDate, tvSubmitter;
        protected ImageView imgPosterImage, imgWatched;

        public MovieViewHolder(View view) {
            super(view);
            view.setTag(this);
            tvTitle = (TextView) view.findViewById(R.id.tv_title_db_list);
            tvReleaseDate = (TextView) view.findViewById(R.id.tv_release_date_db_list);
            tvSubmitter = (TextView) view.findViewById(R.id.tv_submitter_db_list);
            imgPosterImage = (ImageView) view.findViewById(R.id.img_poster_thumbnail_db_list);
            imgWatched = (ImageView) view.findViewById(R.id.img_watched);
        }
    }

}
