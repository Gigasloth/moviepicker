package com.gigasloth.movienight.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.gigasloth.movienight.MovieNight;
import com.gigasloth.movienight.R;
import com.gigasloth.movienight.obj.Movie;
import com.gigasloth.movienight.ui.fragment.DbListFragment;
import com.gigasloth.movienight.ui.fragment.PickerFragment;
import com.gigasloth.movienight.ui.fragment.SearchFragment;

public class ContainerActivity extends AppCompatActivity {
    private MenuItem menuItemAdd, menuItemList, menuItemMarkWatched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PickerFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_picker, menu);
        menuItemAdd = menu.findItem(R.id.action_add_movie);
        menuItemList = menu.findItem(R.id.action_movielist);
        menuItemMarkWatched = menu.findItem(R.id.action_mark_watched);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_movie:
                menuItemAdd.setVisible(false);
                menuItemList.setVisible(false);
                menuItemMarkWatched.setVisible(false);

                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new SearchFragment())
                        .addToBackStack(null)
                        .commit();

                break;
            case R.id.action_mark_watched:
                MovieNight.getCurrentMovieRef().orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Movie currentMovie = dataSnapshot.getValue(Movie.class);

                        if (currentMovie == null) {
                            return;
                        }

                        setWatched(currentMovie);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });

                // todo: add img filter "WATCHED"

                break;
            case R.id.action_movielist:
                menuItemAdd.setVisible(false);
                menuItemList.setVisible(false);
                menuItemMarkWatched.setVisible(false);

                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new DbListFragment())
                        .addToBackStack(null)
                        .commit();

                break;
            case R.id.action_clear_db:
                MovieNight.getMoviesRef().removeValue();
                MovieNight.getCurrentMovieRef().removeValue();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            menuItemAdd.setVisible(true);
            menuItemList.setVisible(true);
            menuItemMarkWatched.setVisible(true);
        } else {
            super.onBackPressed();
        }
    }

    private void setWatched(Movie movie) {
        // Update the movie as watched
        movie.setWatched(true);
        // Update the db
        MovieNight.getMoviesRef().child(movie.getTitle() + " - " + movie.getMovieId()).setValue(movie);
    }
}
