package com.gigasloth.movienight.ui.fragment;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import info.movito.themoviedbapi.TmdbSearch;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;

import com.gigasloth.movienight.MovieNight;
import com.gigasloth.movienight.R;
import com.gigasloth.movienight.obj.Movie;
import com.gigasloth.movienight.ui.component.SearchListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * This fragment will hold the main interface for adding/picking movies at random.
 */
public class SearchFragment extends Fragment implements TextView.OnEditorActionListener {
    private EditText etSearch;
    private RecyclerView rvResults;
    private ArrayList<Movie> movies;
    private SearchListAdapter searchListAdapter;


    public SearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        etSearch = (EditText) rootView.findViewById(R.id.et_search);
        rvResults = (RecyclerView) rootView.findViewById(R.id.rv_results);

        movies = new ArrayList<>();
        rvResults.setLayoutManager(new LinearLayoutManager(MovieNight.context));
        searchListAdapter = new SearchListAdapter(movies);
        rvResults.setAdapter(searchListAdapter);

        etSearch.setOnEditorActionListener(this);

        return rootView;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_DOWN) {
            search();
            return true;
        }

        return false;
    }

    private void search() {
        new AsyncTask<Void, Void, Void>() {

            String searchString;

            @Override
            public void onPreExecute() {
                movies.clear();
                searchListAdapter.notifyDataSetChanged();
                searchString = etSearch.getText().toString();
            }

            @Override
            public Void doInBackground(Void... params) {
                TmdbSearch search = new TmdbSearch(MovieNight.tmdbApi);
                MovieResultsPage results = search.searchMovie(searchString, null, null, false, null);
                ArrayList<MovieDb> movieResults = (ArrayList<MovieDb>) results.getResults();

                addMovies(movieResults);
                return null;
            }

            @Override
            public void onPostExecute(Void result) {
                searchListAdapter.notifyDataSetChanged();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }



    private void addMovies(List<MovieDb> movies) {
        for (MovieDb movie : movies) {
            this.movies.add(new Movie(
                    movie.getId(),
                    movie.getImdbID(),
                    MovieNight.getPrefs().getString(MovieNight.PREF_KEY_USERNAME, null),
                    movie.getTitle(),
                    movie.getReleaseDate(),
                    movie.getPosterPath()));
        }
    }
}
