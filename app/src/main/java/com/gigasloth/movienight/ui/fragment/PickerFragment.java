package com.gigasloth.movienight.ui.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.gigasloth.movienight.MovieNight;
import com.gigasloth.movienight.R;
import com.gigasloth.movienight.obj.Movie;

import java.util.ArrayList;
import java.util.Random;

public class PickerFragment extends Fragment implements View.OnClickListener {
    private TextView tvTitle, tvRelease, tvSubmitter;
    private ImageView imgPoster;
    private FloatingActionButton fabShuffle;

    public PickerFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_picker, container, false);

        tvTitle = (TextView) rootView.findViewById(R.id.tv_movie);
        tvRelease = (TextView) rootView.findViewById(R.id.tv_movie_release);
        tvSubmitter = (TextView) rootView.findViewById(R.id.tv_submitter);
        imgPoster = (ImageView) rootView.findViewById(R.id.img_poster);
        fabShuffle = (FloatingActionButton) rootView.findViewById(R.id.fab_action_shuffle);
        fabShuffle.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!MovieNight.getPrefs().contains(MovieNight.PREF_KEY_USERNAME)) {
            View addUserView = LayoutInflater.from(MovieNight.context)
                    .inflate(R.layout.alert_user_add, null);

            final EditText etUsername = (EditText) addUserView.findViewById(R.id.et_user_add);

            AlertDialog addUserAlert = new AlertDialog.Builder(getActivity())
                    .setView(addUserView)
                    .setTitle(getString(R.string.alert_user_add_title))
                    .setCancelable(true)
                    .setPositiveButton(getString(R.string.alert_user_add), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MovieNight.getPrefs().edit()
                                    .putString(MovieNight.PREF_KEY_USERNAME, etUsername.getText().toString()).commit();
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            getActivity().finish();
                        }
                    })
                    .create();
            addUserAlert.show();
        }

        // If there is a current movie selected, show it
        MovieNight.getCurrentMovieRef().orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Movie currentMovie = dataSnapshot.getValue(Movie.class);

                if (currentMovie == null) {
                    return;
                }

                tvTitle.setText(currentMovie.getTitle());
                tvRelease.setText(currentMovie.getReleaseDate());
                tvSubmitter.setText(currentMovie.getSubmitter());
                getPosterImage(currentMovie.getPosterPath(), imgPoster);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_action_shuffle:
                final ArrayList<Movie> movies = new ArrayList<>();

                MovieNight.getMoviesRef().orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Movie dbitem = snapshot.getValue(Movie.class);

                            if (!dbitem.isWatched()) {
                                movies.add(dbitem);
                            }
                        }

                        if (movies.isEmpty()) {
                            Toast.makeText(MovieNight.context, R.string.toast_empty_db, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Random random = new Random(System.currentTimeMillis());
                        int pick = random.nextInt(movies.size());

                        Movie selectedMovie = movies.get(pick);
                        tvTitle.setText(selectedMovie.getTitle());
                        tvRelease.setText(selectedMovie.getReleaseDate());
                        tvSubmitter.setText(selectedMovie.getSubmitter());
                        getPosterImage(selectedMovie.getPosterPath(), imgPoster);

                        // Set this movie as the active movie
                        MovieNight.getCurrentMovieRef().setValue(selectedMovie);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                    }
                });

                break;
        }
    }

    private void getPosterImage(final String moviePath, final ImageView imageView) {
        MovieNight app = MovieNight.getInstance();
        // https://image.tmdb.org/t/p/
        String requestUrl = MovieNight.config.getSecureBaseUrl() + "/original/" + moviePath;

        app.getImageLoader().get(requestUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer imageContainer, boolean isImmediate) {
                Bitmap bitmap;

                // ImageLoader's first pass checks the cache which will return null first time around
                if ((bitmap = imageContainer.getBitmap()) == null) {
                    return;
                }

                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VOLLEY", volleyError.toString());
            }
        });
    }
}
